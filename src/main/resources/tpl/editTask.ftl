[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey="phing" /][/#assign]

[@ww.select
    name="runtime" labelKey="phing.runtime" cssClass="builderSelectWidget"
    list=uiConfigSupport.getExecutableLabels("phing") extraUtility=addExecutableLink
    required=true
/]

[@ww.textfield name="target" labelKey="phing.target" cssClass="long-field" required=true /]
[@ww.textfield name="config" labelKey="phing.config" cssClass="long-field" /]

[@ww.textarea name="arguments" labelKey="phing.arguments" cssClass="long-field" rows="4" /]
[@ww.textfield name="workingSubDirectory" labelKey="builder.common.sub" cssClass="long-field" /]

[#if !deploymentMode]
    [@ui.bambooSection titleKey="builder.common.tests.directory.description"]
        [@ww.checkbox labelKey="builder.common.tests.exists" name="testChecked" toggle="true" /]

        [@ui.bambooSection dependsOn="testChecked" showOn="true"]
            [@ww.textfield labelKey="builder.common.tests.directory.custom" name="testResultsDirectory" cssClass="long-field" /]
        [/@ui.bambooSection]
    [/@ui.bambooSection]
[#else]
    <p>[@ww.text name='builder.common.deployment.test.disabled' /]</p>
[/#if]